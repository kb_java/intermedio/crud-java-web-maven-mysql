package com.huacachi.proyecto_test_oracle.ConexionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionOracle {
	
	public static Connection getConnection(String jdbcUrl, String jdbcUserName, String jdbcPassword) throws SQLException {
		Connection jdbcConnection;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			jdbcConnection = DriverManager.getConnection(jdbcUrl, jdbcUserName, jdbcPassword);			
		} catch (Exception e) {
			throw new SQLException(e);
		}		
		return jdbcConnection;			
	}
}
