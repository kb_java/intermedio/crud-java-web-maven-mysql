package com.huacachi.proyecto_test_oracle.ConexionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionMysql {
	private String jdbcUrl;
	private String jdbcUserName;
	private String jdbcPassword;
	private Connection jdbcConnection;
	
	public ConexionMysql(String jdbcUrl,String jdbcUserName, String jdbcPassword) {
		// TODO Auto-generated constructor stub
		this.jdbcUrl = jdbcUrl;
		this.jdbcUserName = jdbcUserName;
		this.jdbcPassword = jdbcPassword;
	}
	
	public void connection() throws SQLException {
		if(jdbcConnection==null || jdbcConnection.isClosed()) {
			try { //Driver de conexion
				Class.forName("com.mysql.cj.jdbc.Driver");			
			} catch (Exception e) {
				throw new SQLException(e); 
				// TODO: handle exception
			}		
			//Cadena de conexion
			jdbcConnection = DriverManager.getConnection(jdbcUrl, jdbcUserName, jdbcPassword);
		}		
	}
	
	public void disconect() throws SQLException {
		if(jdbcConnection != null && !jdbcConnection.isClosed()) {
			jdbcConnection.close();			
		}		
	}
	
	public Connection getjdbcConnection() {
		return jdbcConnection;
	}
	
	
		
	

}
