package com.huacachi.proyecto_test_oracle.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.huacachi.proyecto_test_oracle.DAO.ProductoDAO;
import com.huacachi.proyecto_test_oracle.modelo.Producto;

/**
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String jdbcUrl;
	private String jdbcUserName;
	private String jdbcPassword;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controller() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	//Asignamos los valores del archivo web.xml
    	jdbcUrl = getServletContext().getInitParameter("jdbcUrl");
    	jdbcUserName = getServletContext().getInitParameter("jdbcUserName");
    	jdbcPassword = getServletContext().getInitParameter("jdbcPassword");
    	System.out.println(jdbcUrl);
    	
    	ProductoDAO.conexion(jdbcUrl, jdbcUserName, jdbcPassword);    	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String opcion = request.getParameter("opcion");
		
		switch (opcion) {
		case "1":
			Producto producto = new Producto(0, "Juguetes", 50, 13, null, null);			
			ProductoDAO.crearProducto(producto, jdbcUrl, jdbcUserName, jdbcPassword);			
			break;
		case "2":
			Producto producto2 = new Producto();
			ProductoDAO.listarProducto(producto2, jdbcUrl, jdbcUserName, jdbcPassword);
		
		case "3":
			Producto producto3 = new Producto(25, "MainBoard", 50, 14, null, null);
			ProductoDAO.actualizarProducto(producto3, jdbcUrl, jdbcUserName, jdbcPassword);
			break;
		case "4":			
			ProductoDAO.eliminarProducto(26, jdbcUrl, jdbcUserName, jdbcPassword);
		default:
			
			break;
		}
		
		
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
