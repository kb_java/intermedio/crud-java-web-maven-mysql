package com.huacachi.proyecto_test_oracle.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.huacachi.proyecto_test_oracle.ConexionBD.ConexionMysql;
import com.huacachi.proyecto_test_oracle.ConexionBD.ConexionOracle;
import com.huacachi.proyecto_test_oracle.modelo.Producto;

public class ProductoDAO {		
	public static void conexion(String jdbcUrl,String jdbcUserName, String jdbcPassword) {		
		try (Connection cn = ConexionOracle.getConnection(jdbcUrl, jdbcUserName, jdbcPassword)){
			if(cn != null) {
				System.out.println("Conexion Exitosa.!");				
			}else {
				System.out.println("Error en la conexion");
			}			
		} catch (Exception e) {
			e.printStackTrace();			
		}		
	}
	
	public static void crearProducto(Producto producto,String jdbcUrl,String jdbcUserName,String jdbcPassword) {
		try (Connection cn = ConexionOracle.getConnection(jdbcUrl, jdbcUserName, jdbcPassword)){
			String sql = "INSERT INTO producto (nombre, cantidad, precio, fecha_creacion, fecha_actualizacion) VALUES(?,?,?,?,?)";
			PreparedStatement preparedStatement = cn.prepareStatement(sql);
			preparedStatement.setString(1, producto.getNombre());
			preparedStatement.setDouble(2, producto.getCantidad());
			preparedStatement.setDouble(3, producto.getPrecio());
			preparedStatement.setDate(4, producto.getFecha_creacion());
			preparedStatement.setDate(5, producto.getFecha_actualizacion());
			
			preparedStatement.executeUpdate();
			System.out.println("Insercion Correcta..!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void listarProducto(Producto producto, String jdbcUrl, String jdbcUserName, String jdbcPassword) {
		try(Connection connection = ConexionOracle.getConnection(jdbcUrl, jdbcUserName, jdbcPassword)) {
			String sql = "SELECT * FROM producto";
			Statement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery(sql);
			
			while(resultSet.next()) {
				producto = new Producto(
						resultSet.getInt("id_pro"), 
						resultSet.getString("nombre"), 
						resultSet.getDouble("cantidad"), 
						resultSet.getDouble("precio"), 
						resultSet.getDate("fecha_creacion"), 
						resultSet.getDate("fecha_actualizacion"));
				System.out.println(producto);
				
			}
			System.out.println("Se List� todos los productos");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void actualizarProducto(Producto producto,String jdbcUrl,String jdbcUserName,String jdbcPassword) {
		try (Connection connection = ConexionOracle.getConnection(jdbcUrl, jdbcUserName, jdbcPassword)) {
			String sql = "UPDATE producto SET nombre = ? WHERE id_pro = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, producto.getNombre());
			preparedStatement.setInt(2, producto.getId_pro());
			
			preparedStatement.executeUpdate();
			System.out.println("Actualizacion Correcta..!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void eliminarProducto(int cod_pro, String jdbcUrl,String jdbcUserName,String jdbcPassword) {
		try (Connection connection = ConexionOracle.getConnection(jdbcUrl, jdbcUserName, jdbcPassword)){
			String sql = "DELETE FROM producto WHERE id_pro = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, cod_pro);
			
			preparedStatement.executeUpdate();
			System.out.println("Se elimin� correctamente");
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	
	
	
	
	
	
}
